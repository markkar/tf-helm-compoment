ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.9"

scalacOptions ++= Seq(
  "-Ymacro-annotations"
)

lazy val root = (project in file("."))
  .settings(
    name := "tf2"
  )

val circeVersion = "0.14.1"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-effect" % "2.5.5",
  "org.typelevel" %% "cats-core"   % "2.7.0",

  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion
)

