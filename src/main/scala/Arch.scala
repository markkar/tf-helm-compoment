import cats.effect.{IO, Resource}
import chart.{Chart, ChartMeta}

trait Arch[TApp[_], TDep[_]] {
  def app: TApp[Unit]
  def depending[A, B](app: TApp[A], dep: TDep[B]): TApp[(A, B)]
}

object Arch {
  type InitApp[_] = Resource[IO, _]
  type InitDep[_] = Resource[IO, _]

  type HelmApp[_] = Chart[_]
  type HelmDep[_] = Chart[_]

  def apply[TApp[_], TDep[_]](implicit arch: Arch[TApp, TDep]): Arch[TApp, TDep] = arch

  implicit val initializeArch: Arch[InitApp, InitDep] = new Arch[InitApp, InitDep] {
    override def app: InitApp[Unit] = Resource.make(IO.unit)(_ => IO.unit)
    override def depending[A, B](app: InitApp[A], dep: InitDep[B]): InitApp[(A, B)] =
      for {
        application <- app
        dependency <- dep
      } yield (application, dependency)
  }

  implicit val helmArch: Arch[HelmApp, HelmDep] = new Arch[HelmApp, HelmDep] {
    override def app: HelmApp[Unit] = Chart(ChartMeta.default, (), List())
    override def depending[A, B](app: HelmApp[A], dep: HelmDep[B]): HelmApp[(A, B)] =
      Chart(app.meta, app.values, app.dependencies ++ List(dep))
  }
}

object ArchSyntax {
  def app[TApp[_]](implicit arch: Arch[TApp, TApp]): TApp[Unit] = arch.app
  def depending[TApp[_], TDep[_], A, B](app: TApp[A], dep: TDep[B])(implicit arch: Arch[TApp, TDep]): TApp[(A, B)] =
    arch.depending(app, dep)
}
