package chart

import io.circe.generic.JsonCodec
import io.circe.{Decoder, Encoder, HCursor, Json}

@JsonCodec
final case class ChartMeta(
                            apiVersion: String,
                            name: String,
                            version: String,
                            kubeVersion: Option[String] = None,
                            description: Option[String] = None,
                            `type`: Option[String] = None,
                            keywords: Option[Seq[String]] = None,
                            home: Option[Home] = None,
                            sources: Option[Seq[URL]] = None,
                          // what to do with dependencies ???
                            maintainers: Option[Seq[ChartMaintainer]] = None,
                            icon: Option[String] = None,
                            appVersion: Option[String] = None,
                            deprecated: Option[Boolean] = None,
                            annotations: Option[ChartAnnotations] = None
                          )

object ChartMeta {
  def default: ChartMeta = ChartMeta("1.1", "default", "1.0.0")
}

@JsonCodec
case class ChartMaintainer(name: String, email: Option[String], url: Option[URL])

@JsonCodec
case class URL(value: String)

object URL {
  def apply(value: String): Option[URL] = {
    val validationRegex = "(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]+\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]+\\.[^\\s]{2,})"

    if (value.matches(validationRegex)) Some(new URL(value))
    else None
  }

  def empty: URL = new URL("")
}

@JsonCodec
case class ChartAnnotations(example: Map[String, String])

case class Home(url: URL)

object Home {
  implicit val homeEncoder: Encoder[Home] = (home: Home) => Json.obj(
    ("home", Json.fromString(home.url.value))
  )

  implicit val homeDecoder: Decoder[Home] = (c: HCursor) => for {
    homeStringUrl <- c.downField("home").as[String]
  } yield Home(URL(homeStringUrl).getOrElse(URL.empty))
}


