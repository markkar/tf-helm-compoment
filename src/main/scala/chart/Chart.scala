package chart

import io.circe._

trait Chart[TValues] {
  def meta: ChartMeta
  def values: TValues
  def dependencies: List[Chart[_]]
}


object Chart {
  def apply[TValues](appMeta: ChartMeta, appValues: TValues, appDeps: List[Chart[_]]): Chart[TValues] =
    new Chart[TValues] {
      override def meta: ChartMeta = appMeta
      override def values: TValues = appValues
      override def dependencies: List[Chart[_]] = appDeps
    }

  def deploy[F[_], TValues : Encoder : Decoder](chart: Chart[TValues]): F[Unit] = ???
}
